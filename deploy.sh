#!/bin/bash

echo -e "\n==== Configuring AAVS Access Layer ====\n"

# Currently, AAVS LMC has to be installed in this directory
# DO NOT CHANGE!
export AAVS_INSTALL_DIRECTORY=/opt/aavs

# Create installation directory tree
function create_install() {

  # Create install directory if it does not exist
  if [ -z "$AAVS_INSTALL" ]; then
    export AAVS_INSTALL=$AAVS_INSTALL_DIRECTORY

    # Check whether directory already exists
    if [ ! -d "$AAVS_INSTALL_DIRECTORY" ]; then
      # Check whether we have write persmission
      parent_dir="$(dirname "$AAVS_INSTALL_DIRECTORY")"
      if [ -w "$parent_dir" ]; then
        mkdir -p $AAVS_INSTALL_DIRECTORY
      else
        sudo mkdir -p $AAVS_INSTALL_DIRECTORY
        sudo chown $USER $AAVS_INSTALL_DIRECTORY
      fi
    fi
  fi

  if [ ! -d "$AAVS_INSTALL/lib" ]; then
    mkdir -p $AAVS_INSTALL/lib
  fi

  if [[ ! ":$LD_LIBRARY_PATH:" == *"aavs"* ]]; then
    export LD_LIBRARY_PATH=$AAVS_INSTALL/lib${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}
  fi

  if [ ! -d "$AAVS_INSTALL/bin" ]; then
    mkdir -p $AAVS_INSTALL/bin
  fi

  if [ -z "$AAVS_BIN" ]; then
    export AAVS_BIN=$AAVS_INSTALL/bin
  fi
}

# Installing required system packages
echo "Installing required system packages"
sudo apt-get -q install --force-yes --yes $(grep -vE "^\s*#" requirements.apt  | tr "\n" " ")

# Create installation directory
create_install
echo "Created installed directory tree"

# Check if AAVS_PATH exists, and if so cd to it
if [ -z $AAVS_INSTALL ]; then
    echo "AAVS_INSTALL not set in termninal"
    exit 1
else
  # Build library
  cmake -DCMAKE_INSTALL_PREFIX=$AAVS_INSTALL/lib ..
  make -B install
fi
popd

# Building python package
pushd python

# Install required python packages
pip install -r requirements.pip

python setup.py install

# Link required scripts to bin directory
FILE=$AAVS_BIN/aavs_tpm_control.py
if [ ! -e $FILE ]; then
  ln -s $PWD/pyaavs/tpm_control.py $FILE
fi

FILE=$AAVS_BIN/aavs_station.py
if [ ! -e $FILE ]; then
  ln -s $PWD/pyaavs/station.py $FILE
fi

popd

# Copying TPM bitfiles
pushd bitfiles
if [ ! -d $AAVS_INSTALL/bitfiles ]; then
  mkdir $AAVS_INSTALL/bitfiles
fi

cp -r *.bit $AAVS_INSTALL/bitfiles
popd

# Set up GIT LFS for bitfiles
git lfs track "*.bit"
