from setuptools import setup, find_packages

setup(
    name='pyaavs',
    version='1.0',
    packages=find_packages(),
    url='https://bitbucket.org/aavslmc/aavs-access-layer',
    license='',
    author='Alessio Magro',
    author_email='alessio.magro@um.edu.mt',
    description='',
    dependency_links=['https://bitbucket.org/lessju/pyfabil/get/master.zip#egg=pyfabil'],
    install_requires=['enum34', 'scapy', 'pyfabil'],
    extras_require={':python_version == "2.7"': ['futures']}
)
